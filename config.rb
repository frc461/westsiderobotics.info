set :build_dir, 'public'
activate :directory_indexes
activate :asciidoc, base_dir: :source, template_dirs: 'asciidoc_templates', template_engine: :erb
ignore 'stylesheets/sass/**/*.sass'

helpers do
  def resource_blocks_for(key, value = nil)
    @rsbs ||= {}
    unless value
      value = key
      key = 'group'
    end
    @rsbs[[key,value]] ||= sitemap.resources.select { |s| s.data.count > 0 && s.data[key] == value }.sort_by {|s| [s.data.position, s.data.title] }
  end
end